import React from 'react'

const TitleGroup = ({children}) => {
    return (
        <div className='w-full border-b py-1 my-2 text-xs font-bold text-slate-700'>{children}</div>
    )
}

export default TitleGroup