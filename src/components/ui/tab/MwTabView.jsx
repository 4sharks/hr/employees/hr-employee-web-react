import React, { Children } from 'react'

const MwTabView = ({children}) => {
  return (
    <div className='flex-1 flex flex-col my-3 items-start '>{children}</div>
  )
}

export default MwTabView