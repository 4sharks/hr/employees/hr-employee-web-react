import React from 'react'

const MwTabViewHead = ({tabTitleList=[],tabActive,setTabActive}) => {
    return (
        <div className='flex text-sm text-slate-600 '>
            {
                tabTitleList.map((tabTitle) =>(
                    <div onClick={()=>setTabActive(tabTitle)} className={`px-3 py-2 ml-1 bg-slate-200 rounded-t-xl ${tabTitle === tabActive && 'bg-slate-300 text-slate-800'}`}> {tabTitle}</div>
                ))
            }
        </div>
    )
}

export default MwTabViewHead