import React, { useEffect, useState } from 'react'
import { MwButton , MwSpinnerButton} from '../ui'
import CardHeaderWithActions from '../ui/CardHeaderWithActions'
import { useCurrent, usePost } from '../../hooks';
import DepartmentForm from './DepartmentForm';

const AddNewDepartment = ({onHide,callback,reload,setReload}) => {
    const [errors,setErrors] = useState({});
    const [saved,setSaved] = useState(false);
    const [departmentName, setDepartmentName] = useState('');
    const [managerSelected, setManagerSelected] = useState({});
    const [parentDepartmentSelected, setParentDepartmentSelected] = useState({});
    const [employeesList, setEmployeesList] = useState([]);
    const [departmentList, setDepartmentList] = useState([]);
    const {currentTenantId,currentCompanyId,currentBranchId,currentLangId,userId} = useCurrent();
    const {data:resultEmployeesData,loading:loadingEmployees,postData:postDataEmployees} = usePost();
    const {data:resultDepartmentsData,loading:loadingDepartments,postData:postDataDepartments} = usePost();
    const {data:resultPost,loading:loadingPost,postData} = usePost();

    const init = async() => {
        const _data = {
            tenant_id:currentTenantId,
            company_id:currentCompanyId,
            branch_id:currentBranchId
        }
        if(currentTenantId && currentCompanyId && currentBranchId){
            if(!loadingEmployees && !resultEmployeesData){
                await postDataEmployees(`${process.env.REACT_APP_HR_EMPLYEE_API_BASE_URL}/employees`,_data);
            }
            if(!loadingDepartments && !resultDepartmentsData){
                await postDataDepartments(`${process.env.REACT_APP_HR_EMPLYEE_API_BASE_URL}/departments`,_data);
            }
        }
    }

    useEffect(() =>{
        init();
    },[]);

    useEffect(() =>{
        if(resultEmployeesData){
            const empList = resultEmployeesData.data.map(employee => ({
                label:employee[`name_${currentLangId}`],
                value:employee.id,
                lang: currentLangId
                
            }));
            setEmployeesList(empList);
        }
    },[resultEmployeesData]);

    useEffect(() =>{
        if(resultDepartmentsData){
            const deptList = resultDepartmentsData.data.map(dept => ({
                label:dept.name,
                value:dept.id,
                lang: currentLangId
                
            }));
            setDepartmentList(deptList);
        }
    },[resultDepartmentsData]);

    useEffect(() => {
        if(resultPost){
            console.log(resultPost);
            setReload(!reload);
            if(onHide){
                setSaved(true);
                onHide();
                callback();
            }
            setDepartmentName('')

        }
    },[resultPost]);

    const saveHandler = async() =>{
        setErrors({});
        if(departmentName){
            const _data = {
                name: departmentName,
                manager_id:managerSelected.value || null,
                parent_id: parentDepartmentSelected.value || null,
                created_by:userId,
                tenant_id:currentTenantId,
                company_id:currentCompanyId,
                branch_id:currentBranchId
            }
            await postData(`${process.env.REACT_APP_HR_EMPLYEE_API_BASE_URL}/departments/add`,_data);
            
        }else{
            setErrors({ departmentName:'يجب ادخال اسم القسم' });
        }
    }

    return (
        <div className='flex flex-col   p-4 '>

            <CardHeaderWithActions>
                { !loadingPost && onHide && <MwButton type='cancelBtn' inGroup={true} onClick={()=>onHide()}>إغلاق</MwButton> }
                { loadingPost ? <MwSpinnerButton/> : <MwButton type='saveBtn' disabled={saved} inGroup={true} onClick={saveHandler}>حفظ</MwButton>}
            </CardHeaderWithActions>

            <div className='text-sm font-bold text-slate-500 p-2'>
                <span> أضف قسم جديد </span>
            </div>

            <DepartmentForm
                errors = {errors}
                departmentName = {departmentName}
                setDepartmentName ={setDepartmentName}
                employeesList = {employeesList}
                managerSelected = {managerSelected}
                setManagerSelected = {setManagerSelected}
                departmentList = {departmentList}
                parentDepartmentSelected = {parentDepartmentSelected}
                setParentDepartmentSelected = {setParentDepartmentSelected}
                />
            
        </div>
    )
}

export default AddNewDepartment