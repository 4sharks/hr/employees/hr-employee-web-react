
import { Routes ,Route } from "react-router-dom";
import withTenant from './utils/withTenant';
import { useState } from "react";
import { useSelector } from "react-redux";

import NotFound from "./pages/NotFound";
import TokenRedirect from "./pages/TokenRedirect";
import Logout from "./pages/Logout";
import DashBoard from "./pages/DashBoard";

import HeaderNavBar from "./layout/HeaderNavBar";
import Footer from "./layout/Footer";
import MenuSide from "./layout/MenuSide";

import withGuard from "./utils/withGuard";
import { useTenant,useStartup } from "./hooks";
import { MwSpinner } from "./components/ui";

import AddNewEmployee from "./pages/employee/AddNewEmployee";
import EditEmployee from "./pages/employee/EditEmployee";
import Employees from "./pages/employee/Employees";
import Settings from "./pages/Settings";
import AttendanceManager from "./pages/attendance/AttendanceManager";
import ManageDepartment from "./pages/ManageDepartment";
import JobPosition from "./pages/JobPosition";

function App() {
  const [openMenu,setOpenMenu] = useState(false);
  const langState = useSelector((state) => state.lang);
  const {tenant,tenantUsername,loading:loadingTenant,companiesList,branchesList,tenantId,companySelected,branchSelected} = useTenant();
  const {startup} = useStartup();
  return (
    !loadingTenant && startup ? <div dir={langState?.value === 'ar' ? 'rtl' : 'ltr'} className="h-screen flex flex-col flex-between  ">
      <HeaderNavBar
        companySelected = {companySelected}
        branchSelected = {branchSelected}
        companiesList = {companiesList}
        branchesList = {branchesList}
        tenantUsername = {tenantUsername}
        tenant = {tenant}
        openMenu = {openMenu}
        setOpenMenu = {setOpenMenu}
      />
      <div className="bg-white flex  flex-1   m-auto  w-full">
        <MenuSide
          openMenu={openMenu}
          setOpenMenu={setOpenMenu}
          />
        <div className={`flex flex-col flex-1 px-2 py-1   primary-bg-color ${openMenu ? 'ms-8 md:ms-0 ps-3' :'md:ms-10'} `}>
          <div className="p-5 rounded-2xl bg-slate-50 min-h-[calc(100vh-90px)]">
            <Routes >
                <Route path="/"   element={ <Employees/>} />
                <Route path=":tenant/"   element={ <Employees/>} />
                <Route path=":tenant/dashboard"   element={ <Employees/>} />
                <Route path=":tenant/token/:token" element={<TokenRedirect/>} />
                <Route path=":tenant/employees/addNewEmployee" element={ <AddNewEmployee/>} />
                <Route path=":tenant/employees/:id" element={ <EditEmployee/>} />
                <Route path=":tenant/employees" element={ <Employees/>} />
                <Route path=":tenant/attendance" element={ <AttendanceManager/>} />
                <Route path=":tenant/departments" element={ <ManageDepartment/>} />
                <Route path=":tenant/job-position" element={ <JobPosition/>} />
                <Route  path=":tenant/logout"  element={ <Logout/> } />
                <Route  path=":tenant/settings"  element={ <Settings/> } />
                <Route path="/notfound"  errorElement={<NotFound/> }  />
                <Route path="/*"  errorElement={<NotFound/> }  />
            </Routes>
          </div>
        </div>
      </div>
      <Footer/>
    </div> : <MwSpinner/>
  );


}

export default withTenant(withGuard(App));
